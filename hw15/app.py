from flask import Flask, redirect, render_template, request
import sqlite3

app = Flask(__name__)


@app.route('/')
def start_page():
    with sqlite3.connect('music') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM music")

        music = cursor.fetchall()

    return render_template('home.html', music=music)


@app.route('/track-delete/', methods=['GET', 'POST'])
def delete_track():
    if request.method == 'POST':
        with sqlite3.connect('music') as connection:
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM music WHERE Id={request.form['track_id']}")
            connection.commit()
    return redirect('/')


@app.route('/track-add/', methods=['GET', 'POST'])
def add_track():
    if request.method == 'POST':
        with sqlite3.connect('music') as connection:
            insert = f'INSERT INTO music (name, author, duration) VALUES ("{request.form["name"]}", ' \
                    f'"{request.form["author"]}", "{request.form["duration"]}")'
            cursor = connection.cursor()
            cursor.execute(insert)
            connection.commit()
        return redirect('/')
    return render_template('add_track.html')


if __name__ == '__main__':
    app.run(debug=True)
